import { createSlice, nanoid, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
    posts: [],
    status: 'idle',
    error: null
}

export const fetchPosts = createAsyncThunk(() => {
    fetch('http://localhost:3000/community', {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
            },
          })
        .then(response => response.json())
        .then(data => {
            console.log(data);
        })
        .catch((error) => {
          console.error('Error:', error);
        });
});


export default fetchPosts.reducer