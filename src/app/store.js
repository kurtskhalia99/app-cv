import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/education/educationSlice';


export const store = configureStore({
    reducer: {
        counter: counterReducer,
    },
})