import './Label.css'
function Label({text}) {
    return ( 
        <h1 id={text} className="app-label">{text}</h1>
     );
}

export default Label;