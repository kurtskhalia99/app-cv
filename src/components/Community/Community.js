import React, { useEffect, useState } from 'react';
import Expertise from '../Expertise/Expertise';
import Feedback from '../Feedback/Feedback';
import Box from '../Box/Box';
import Panel from '../Panel/Panel';
import './Community.css';
import Portfolio from '../Portfolio/Portfolio';
import TimeLine from '../Timeline/Timeline';
import Address from '../Address/Address';
import Skills from '../Skills/Skills';

function Community() {
    const [education, setEducation] = useState([]);
    const [spinner, setSpinner] = useState(true);

    
    useEffect(() => {
        fetch('/api/educations', {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
            },
          })
        .then(response => response.json())
        .then(data => {
            setEducation(data.educations);
            setSpinner(false);
        })
        .catch((error) => {
            setEducation(['Something went wrong; please review your server connection!']);
        });
    },[]);

    return ( 
        <div>
            <div className="app-container">
                <Panel></Panel>
                <div className='app-main'>
                    <Box id="about" title='About me' content='I am an enthusiastic and detail-oriented Front-End developer seeking an entry-level position with Company to use my skills in coding, troubleshooting complex problems, where I can grow and learn from other experienced team members.' />
                    <TimeLine data={education} spinner={spinner} />
                    <Expertise data={[
                                    {
                                        date: '2020-2021', 
                                        info: {
                                        company: 'JTI',
                                        job: 'Sales analyst',
                                        description: 'Sales analyst'
                                        }
                                    },
                                    {
                                        date: '2019-2022', 
                                        info: {
                                        company: 'Freelancer',
                                        job: 'Accountant',
                                        description: 'Accountant'
                                        }
                                    }
                                    ]} 
                    />
                    <Skills />
                    <Portfolio />
                    <Address />
                    <Feedback data={[ {feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', reporter: { photoUrl: 'https://www.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg', name: 'John Doe', citeUrl: 'https://www.citeexample.com' } }, {feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', reporter: { photoUrl: 'https://www.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg', name: 'John Doe', citeUrl: 'https://www.citeexample.com' } } ]} />
                </div>
                </div>
        </div>
     );
}

export default Community;