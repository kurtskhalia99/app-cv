import React from 'react';
import Label from '../Label/Label';

function Spinner({labelText, content}) {
    return ( 
        <div className='app-timeline-spinner'>
            <Label text={labelText} />
            <div className='app-timeline-spinner-content'>
                {content}
            </div>
        </div>
     );
}

export default Spinner;