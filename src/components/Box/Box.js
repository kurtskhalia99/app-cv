import React from 'react';
import Label from '../Label/Label';

function Box({title, content}) {
    return ( 
        <div>
            <Label text={title} />
            <p>{content}</p>
        </div>
     );
}

export default Box;
