import React from 'react';
import './Expertise.css';
import Label from '../Label/Label';

function Expertise({data}) {
        return (  
            <div>
                <Label text='Experience' />
                <ul>
                    {
                        data.map((item) => {
                        return  <li key={item.info.company} className='app-expertise-item'>
                                    <div className='app-expertise-item--name'>
                                        <h3>{item.info.company}</h3>
                                        <p>{item.date}</p>
                                    </div>
                                    <div className='app-expertise-item--desc'>
                                        <h3>{item.info.job}</h3>
                                        <p>{item.info.description}</p>
                                    </div>
                                </li>
                        })
                    }
                </ul>
            </div>
         );
}

export default Expertise;