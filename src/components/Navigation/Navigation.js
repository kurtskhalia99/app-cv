import React from 'react';
import {Link} from 'react-scroll';
import './Navigation.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function Navigation() {
    return (
      <div className='app-navigation'>
        <nav className="app-navigation-nav">
          <ul className="app-navigation-list">
            <Link activeClass="active" smooth spy to="About me">
              <li className="app-navigation-list-item">
                <FontAwesomeIcon
                  icon="fa-solid fa-user"
                  className="app-navigation-list-item-icon"
                />
                <span className="app-navigation-list-item-text">About me</span>
              </li>
            </Link>
            <Link activeClass="active" smooth spy to="Education">
              <li className="app-navigation-list-item">
                  <FontAwesomeIcon
                    icon="fa-solid fa-graduation-cap"
                    className="app-navigation-list-item-icon"
                  />
                <span className="app-navigation-list-item-text">Education</span>
              </li>
            </Link>
            <Link activeClass="active" smooth spy to="Experience">
              <li className="app-navigation-list-item">
                  <FontAwesomeIcon
                    icon="fa-solid fa-pen"
                    className="app-navigation-list-item-icon"
                  />
                <span className="app-navigation-list-item-text">Experience</span>
              </li>
            </Link>
            <Link activeClass="active" smooth spy to="Portfolio">
              <li className="app-navigation-list-item">
                  <FontAwesomeIcon
                    icon="fa-solid fa-suitcase"
                    className="app-navigation-list-item-icon"
                  />
                <span className="app-navigation-list-item-text">Portfolio</span>
              </li>
            </Link>
            <Link activeClass="active" smooth spy to="Contacts">
              <li className="app-navigation-list-item">
                  <FontAwesomeIcon
                    icon="fa-solid fa-location-arrow"
                    className="app-navigation-list-item-icon"
                  />
                <span className="app-navigation-list-item-text">Contacts</span>
              </li>
            </Link>
            <Link activeClass="active" smooth spy to="Feedbacks">
              <li className="app-navigation-list-item">
                  <FontAwesomeIcon
                    icon="fa-solid fa-comment"
                    className="app-navigation-list-item-icon"
                  />
                <span className="app-navigation-list-item-text">Feedbacks</span>
              </li>
            </Link>
          </ul>
        </nav>
      </div>
    );
}

export default Navigation;