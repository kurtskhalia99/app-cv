import React, { useState } from 'react';
import {Link} from "react-router-dom";
import Navigation from '../Navigation/Navigation';
import Button from '../Button/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Panel.css';
import PhotoBox from '../PhotoBox/PhotoBox';
import avatar from '../../assets/images/avatar.jpg';



function Panel() {
    const [panel, setPanel] = useState(true);
    const handleClick = () => {
        setPanel(!panel);
    }
    if(!panel){
        return (
            <div className='app-panel-menu-bars' style={{left:0}} onClick={handleClick}>
                <FontAwesomeIcon icon="fa-solid fa-bars" />
            </div>
        )
    } else {
    return (
        <div className='app-panel' >
            <div className='app-panel-left'>
            <div className="app-left-side" >
                <PhotoBox
                    name="Nika Kurtskhalia"
                    title=""
                    description=""
                    avatar={avatar} 
                />
                <Navigation />
                <Link to="/">
                <Button
                    icon={
                    <FontAwesomeIcon
                        className="app-navigation-list-item-icon"
                        icon="chevron-left"
                    />
                    }
                    text="Go Back"
                ></Button>{" "}
                </Link>
            </div>
            <div className='app-panel-menu-bars' onClick={handleClick}>
                <FontAwesomeIcon icon="fa-solid fa-bars" />
            </div>
            </div>
        </div>
    );
                }
}

export default Panel;