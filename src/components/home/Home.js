import React from 'react';
import avatar from '../../assets/images/avatar.jpg';
import {Link} from "react-router-dom";
import Button from '../Button/Button';
import PhotoBox from '../PhotoBox/PhotoBox';
import './Home.css';

function Home() {
    return ( 
        <div className='app-main-page'>
            <div className='app-main-page--container'>
                <PhotoBox
                    name="Nika Kurtskhalia"
                    title="Programmer"
                    description="Frontend developer"
                    avatar={avatar} 
                />
                <Link to='/nika'><Button text='Know more'></Button></Link>
            </div>
        </div>
     );
}

export default Home;