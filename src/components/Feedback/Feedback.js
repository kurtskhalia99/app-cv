import React from 'react';
import './Feedback.css';
import Info from '../Info/Info';
import Label from '../Label/Label';

function Feedback({data}) {
    return ( 
        <div>
            <Label text='Feedbacks' />
            <ul>
                {
                    data.map((item, i) => {
                    return  <li key={i} className='app-feedback-item'>
                                <blockquote>
                                    <Info text={item.feedback}/>
                                    <div className='app-feedback-item-reporter'>
                                        <img className='app-feedback-item-photo' alt='reporter img' src={item.reporter.photoUrl}></img>
                                        <cite>
                                            {item.reporter.name} , 
                                            <a className='app-feedback-item-url' href={item.reporter.citeUrl}>{item.reporter.citeUrl}</a>
                                        </cite>
                                    </div>
                                </blockquote>
                            </li>
                    })
                }
            </ul>
        </div>
     );
}

export default Feedback;