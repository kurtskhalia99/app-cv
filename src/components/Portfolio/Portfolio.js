import React, {useEffect, useRef} from 'react';
import PortfolioInfo from './PortfolioInfo';
import Isotope from 'isotope-layout';
import './Portfolio.css';
import Label from '../Label/Label';


function Portfolio() {
    const isotope = useRef();
    const [filterKey, setFilterKey] = React.useState('*');

    useEffect(() => {
        isotope.current = new Isotope('.app-portfolio-content', {
          itemSelector: '.app-portfolio-filter-item',
        })
        return () => isotope.current.destroy()
    }, []);

    useEffect(() => {
        filterKey === '*'
          ? isotope.current.arrange({filter: `*`})
          : isotope.current.arrange({filter: `.${filterKey}`})
    }, [filterKey]);

    const handleFilterKeyChange = key => () => setFilterKey(key);

    return ( 
        <div className='app-portfolio'>
            <Label text='Portfolio' />
            <ul className='app-portfolio-filter-tabs'>
                <li className='app-portfolio-filter-tab' onClick={handleFilterKeyChange('*')}>All /</li>
                <li className='app-portfolio-filter-tab' onClick={handleFilterKeyChange('Ui')}>Ui /</li>
                <li className='app-portfolio-filter-tab' onClick={handleFilterKeyChange('Code')}>Code</li>
            </ul>
            <hr />
            <ul className="app-portfolio-content">
                <li className="app-portfolio-filter-item Ui">
                    <PortfolioInfo title="Some text" text="Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo" url="https://somesite.com" />
                </li>
                <li className="app-portfolio-filter-item Code">
                    <PortfolioInfo title="Some text" text="Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo" url="https://somesite.com" />
                </li>
                <li className="app-portfolio-filter-item Ui">
                    <PortfolioInfo title="Some text" text="Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo" url="https://somesite.com" />
                </li>
                <li className="app-portfolio-filter-item Ui Code">
                    <PortfolioInfo title="Some text" text="Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo" url="https://somesite.com" />
                </li>
            </ul>
        </div>
     );
}

export default Portfolio;