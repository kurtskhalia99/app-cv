import React from 'react';
import './PortfolioInfo.css';

function PortfolioInfo({title, text, url}) {
    return ( 
        <div className='app-portfolio-portfolioinfo'>
            <h3>{title}</h3>
            <p>{text}</p>
            <a href={url}>View source</a>
        </div>
     );
}

export default PortfolioInfo;