import './App.css';
import Home from '../home/Home';
import {Routes, Route} from "react-router-dom";
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import Community from '../Community/Community'

library.add(fas);
library.add(fab);

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<Home/>}></Route>
        <Route path='/nika' element={<Community />}></Route>
      </Routes>
    </div>
  );
}

export default App;
