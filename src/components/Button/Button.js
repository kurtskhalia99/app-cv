import React from 'react';
import './Button.css';

function Button(props) {
    return (
      <button className="app-main-page--button">
        {" "}
        <span className="text">
          {props.icon ? props.icon : ""} <span className='app-navigation-list-item-text'>{props.text}</span>
        </span>{" "}
      </button>
    );
}

export default Button;