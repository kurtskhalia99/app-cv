import React from 'react';
import './PhotoBox.css';

function PhotoBox({name,title,description,avatar}) {
    return ( 
        <div className='app-photobox'>
            <img alt='avatar' className='app-main-page--avatar' src={avatar}></img>
            <h1 className='app-main-page--name text'>{name}</h1>
            <h3 className='app-main-page--position text'>{title}</h3>
            <p className='app-main-page--information text'>{description}</p>
        </div>
     );
}

export default PhotoBox;