import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Address.css';
import Label from '../Label/Label';

function Address() {
    return ( 
        <div className='app-address'>
            <Label text='Contacts' />
            <div className='app-address-phone app-address-item'>
                <FontAwesomeIcon className="app-address-icon" icon="fa-solid fa-phone" />
                <div>
                    <label className='app-address-item-label'><a href="tel:+995551508596">551 50 85 96</a></label>
                </div>
            </div>
            <div className='app-address-mail app-address-item'>
                <FontAwesomeIcon className="app-address-icon" icon="fa-solid fa-envelope" />
                <div className="app-address-item-text">
                    <label className='app-address-item-label'>Gmail</label>
                    <a href='mailto:kurtskhalia99@gmail.com'>kurtskhalia99@gmail.com</a>
                </div>
            </div>
            <div className='app-address-facebook app-address-item'>
                <FontAwesomeIcon className="app-address-icon" icon="fa-brands fa-facebook-f" />
                <div className="app-address-item-text">
                    <label className='app-address-item-label'>Facebook</label>
                    <a href='https://www.facebook.com/kurcxni/'>https://www.facebook.com/kurcxni/</a>
                </div>
            </div>
            <div className='app-address-linkedin app-address-item'>
                <FontAwesomeIcon className="app-address-icon" icon="fa-brands fa-linkedin-in" />
                <div className="app-address-item-text">
                    <label className='app-address-item-label'>Linkedin</label>
                    <a href='https://www.linkedin.com/in/nika-kurtskhalia-718b3a202/'>https://www.linkedin.com/in/nika-kurtskhalia-718b3a202/</a>
                </div>
            </div>
            <div className='app-address-github app-address-item'>
                <FontAwesomeIcon className="app-address-icon" icon="fa-brands fa-github" />
                <div className="app-address-item-text">
                    <label className='app-address-item-label'>Github</label>
                    <a href='https://github.com/kurtskhala'>https://github.com/kurtskhala</a>
                </div>
            </div>
        </div>
     );
}

export default Address;