import React, { useEffect, useState } from 'react';
import { Formik, Field, Form } from 'formik';
import Label from '../Label/Label';
import './Skills.css';
import Button from '../Button/Button';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';


function validate(value) {
    let error;
    if (value.length > 3) {
      error = "max digits 3";
    } else if (parseInt(value) > 100 || parseInt(value) < 10) {
      error = "range is 1 to 100";
    }
    return error;
}

function Skills() {
    const [addSkills, setAddSkills] = useState(true);
    const handleClick = () => {
        setAddSkills(!addSkills);
    }
    const [skill, setSkill] = useState([]);
    const [skillLoad, setSkillLoad] = useState(true);

    const fetchData = () => {
        fetch('/api/skills', {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
            },
          })
        .then(response => response.json())
        .then(data => {
            setSkill(data.skills);
            setSkillLoad(false);
        })
        .catch((error) => {
            console.log(error);
        });
    }

    useEffect(() => {
        fetchData();
    },[]);
    return (
        <div className='app-skills'>
            <div className='app-skills-top'>
                <Label text="Skills" />
                <span onClick={handleClick}>
                    <Button icon={<FontAwesomeIcon icon="fa-solid fa-pen" />} text='Open edit'/>
                </span>
            </div>
            <span style={{display: addSkills ? 'none' : 'block' }}>
                <Formik 
                    initialValues={{
                        name: '',
                        range: ''
                    }}
                    onSubmit={ (values) => {
                        fetch('/api/skills', {
                            method: 'POST',
                            headers: {
                            'Content-Type': 'application/json',
                            },
                            body: JSON.stringify(values),
                        })
                        .then((response) => { response.json() })
                        .then(data => {
                            console.log(data);
                        })
                        .catch((error) => {
                            console.error('Error:', error);
                        });
                        fetchData();
                    }}
                    >
                    <Form className='app-skills-formik'>
                        <div className='app-skills-form-comp'>
                            <label className='app-skills-form-label' htmlFor="name">Skill name:</label>
                            <Field className='app-skills-form-field' id="name" name="name" placeholder="Enter skill name" />
                        </div>
                        <div className='app-skills-form-comp'>
                            <label className='app-skills-form-label' htmlFor="range">Skill range:</label>
                            <Field validate={validate} type='number' className='app-skills-form-field' id="range" name="range" placeholder="Enter skill range" />
                        </div>
                        <Button text='Add skill'/>
                    </Form>
                </Formik>
            </span>
            {!skillLoad?
                <div className='app-skills-graph'>
                    {
                        skill.map((skill) => {
                            return <div style={{width : `${skill.range}%`}} className='app-skill' key={skill.id}><span className='app-skill-name'>{skill.name}</span></div>
                            console.log(skill);
                        })
                    }
                    <div className='app-skills-line'><span></span><span></span><span></span><span></span></div>
                    <div className='app-skills-xAxis'>
                        <span className='app-skills-xAxis-text'>Beginner</span>
                        <span className='app-skills-xAxis-text'>Proficient</span>
                        <span className='app-skills-xAxis-text'>Expert</span>
                        <span className='app-skills-xAxis-text'>Master</span>
                    </div>
                </div>
                :
                <div></div>
            }
        </div>
     );
}

export default Skills;