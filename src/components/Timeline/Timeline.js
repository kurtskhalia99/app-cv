import React from 'react';
import './Timeline.css';
import Label from '../Label/Label';
import Spinner from '../Spinner/Spinner';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function TimeLine({data, spinner}) {
    if(spinner){    
        return (
            <Spinner labelText ='Education' content={<FontAwesomeIcon className="fas fa-spinner fa-spin app-timeline-spinner-icon" icon="sync-alt" />} />
        )
    } else {
        if(!data[0].date) {
            return (
                <Spinner labelText='Education' content ={ <h3 className='app-timeline-error-text'>{data[0]}</h3>} />
            )
        } else {
            return ( 
                <div className='app-timeline'>
                    <Label text='Education' />
                    <ul className='app-timeline-list'>
                        {
                            data.map((item) => {
                            return  <li key={item.title} className='app-timeline-item'>
                                        <legend className='app-timeline-item--date'>
                                            <h3>{item.date}</h3>
                                        </legend>
                                        <div className='app-timeline-item--desc'>
                                            <h3 className='app-timeline-item--title'>{item.title}</h3>
                                            <p className='app-timeline-item--text'>{item.text}</p>
                                        </div>
                                    </li>
                            })
                        }
                    </ul>
                </div>
            );
        }
    }
}

export default TimeLine;