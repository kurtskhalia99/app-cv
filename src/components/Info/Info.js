import React from 'react';
import './Info.css'

function Info({text}) {
    return ( 
        <div className='app-info-text'>
            <p className='app-info-text-p'>
                {text}
            </p>
        </div>
     );
}

export default Info;