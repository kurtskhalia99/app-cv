import { createServer, Model } from "miragejs";

export function makeServer({ environment = 'test' } = {}) {
    let server = createServer({
        environment,
        models: {
            skills: Model,
        },
        seeds(server) {
            server.create('skill', {
                name: 'JavaScript',
                range: 90
             });
            server.create('skill', {
                name: 'TypeScript',
                range: 70
             });
            server.create('skill', {
                name: 'HTML',
                range: 100
             });
            server.create('skill', {
                name: 'CSS',
                range: 90
             });
            server.create('skill', {
                name: 'language extension - SCSS',
                range: 70
             });
            server.create('skill', {
                name: 'PHP',
                range: 50
             });
            server.create('skill', {
                name: 'Java',
                range: 50
             });
            server.create('skill', {
                name: 'React',
                range: 80
             });
            server.create('skill', {
                name: 'Redux',
                range: 70
             });
            server.create('skill', {
                name: 'Oracle SQL',
                range: 90
             });
            server.create('skill', {
                name: 'MySQL',
                range: 90
             });
            server.create('skill', {
                name: 'GIT',
                range: 100
             });
            server.create('skill', {
                name: 'Visual Studio',
                range: 100
             });
            server.create('skill', {
                name: 'Frameworks: Bootstrap',
                range: 80
             });
            server.create('skill', {
                name: 'Frameworks: Semantic U',
                range: 80
             });
        },
        routes() {
            this.namespace = "api";
            this.timing = 3000;
    
            this.get('/educations', () => ({
                educations : [
                    {
                        "date": 2021,
                        "title": "Free University of Tbilisi",
                        "text": "Bachelor of Business Administration (ESM) Relevant coursework in Computer Programming: Programming Methodologies, Web Development – JavaScript, Working with Databases - SQL"
                    },
                    {
                        "date": 2022,
                        "title": "Udemy - The Web Developer Bootcamp 2022",
                        "text": "The only course you need to learn web development - HTML, CSS, JS, Node, and More!"
                    }
                    ,
                    {
                        "date": 2022,
                        "title": "Udemy - Modern React with Redux",
                        "text": "Master React and Redux with React Router, Webpack, and Create-React-App. Includes Hooks!"
                    }
                    ,
                    {
                        "date": 2022,
                        "title": "Linkedin Learning - React.js Essential Training (2020)",
                        "text": "In this course, I was introduced the basics of the React library using the most modern syntax and best practices for creating React components. Along the way, learn how to set up Chrome tools for React; create new components; work with the built-in Hooks in React; and more. "
                    }
                    ,
                    {
                        "date": 2022,
                        "title": "Linkedin Learning - Building Modern Projects with React",
                        "text": "Learn about some of the most important tools in the React ecosystem that help you simplify development and manage the complexities of state, side effects, structure, and styling. Instructor Shaun Wassell shows how to create a basic React project, and then explains how to add Redux, create thunks, use selectors, work with styled-components, perform testing, and more."
                    }
                    ,
                    {
                        "date": 2022,
                        "title": "Linkedin Learning - TypeScript Essential Training",
                        "text": "In this course, Jess Chadwick teaches you how to leverage the full power of the TypeScript language in your JavaScript applications, starting by revisiting some JavaScript fundamentals. Then he reviews the data types, classes, generics, modules, and decorators that are unique to TypeScript."
                    }
                    ,
                    {
                        "date": 2022,
                        "title": "Linkedin Learning - JavaScript: Ajax and Fetch",
                        "text": "This course introduces both modern and legacy approaches for requesting and handling data and modifying webpage content with Ajax and vanilla JavaScript. JS expert Sasha Vodnik shows how to work with the Fetch and XMLHttpRequest (XHR) APIs, so you can see how Ajax is implemented in different codebases. He also shows how to optimize Ajax requests and responses within the browser, modify webpage content through the DOM, and handle any errors that result. Plus, learn how to move your API keys out of front end code and onto a proxy server to keep them secure."
                    }
                    ,
                    {
                        "date": 2022,
                        "title": "Linkedin Learning - Learning Git and GitHub",
                        "text": "In this course, Kevin Skoglund explores the fundamental concepts behind version control systems and the Git architecture. Using a step-by-step approach, he shows how to install Git and presents the commands that enable efficient code management. Learn how to add, change, and delete files in the repository; view a log of previous commits; and compare versions of a file. Plus, see how to undo changes to files and ignore certain files in a Git repository."
                    }
                    ,
                    {
                        "date": 2022,
                        "title": "Linkedin Learning - Vanilla JavaScript: Web Performance Optimization APIs",
                        "text": "This course shows how to collect real metrics from real devices with the four most useful APIs: Performance Timeline, Navigation Timing, User Timing, and Resource Timing."
                    }
                    ,
                    {
                        "date": 2022,
                        "title": "Linkedin Learning - JavaScript: Client-Side Data Storage",
                        "text": "In this course, Emmanuel Henri provides coverage of key client-side storage solutions for JavaScript developers. He goes over the basics of local and session storage and provides use cases that can help you determine when to opt for one type of web storage over the other. He also discusses how to set up and work with IndexedDB, including how to add, retrieve, and delete items with this powerful API. Learn about localForage and other options for handling the local storage of large files."
                    }
                    ,
                    {
                        "date": 2022,
                        "title": "Linkedin Learning - ESLint: Integrating with Your Workflow",
                        "text": "In this concise course—the first in the ESLint series—learn how to integrate ESLint into your workflow to customize automatic error checking."
                    }
                    ,
                    {
                        "date": 2022,
                        "title": "Linkedin Learning - CSS Layouts: From Float to Flexbox and Grid",
                        "text": "In this course, Christina Truong guides you though this process, from initial concepts to complete conversion, highlighting the different ways to create page layouts with modern CSS. Explore how to work with the display and float properties; use relative, absolute, and fixed positioning for laying out components; create basic Grid and Flexbox layouts; and more."
                    }
                    ,
                    {
                        "date": 2022,
                        "title": "Linkedin Learning - Sass Essential Training",
                        "text": "The fundamentals of Syntactically Awesome Stylesheets (Sass), a modern web development language that helps you write CSS better, faster, and with more advanced features. Ray Villalobos shows you the best way to install Sass and work with its main features: variables, nesting, partials, and mixins."
                    }
                ]
            }))
            this.get('/skills', (schema) => {
                return schema.skills.all();
            })
            this.post("/skills", (schema, request) => {
                let attrs = JSON.parse(request.requestBody);
                return schema.skills.create(attrs);
            })
        }
    });
    return server;
}