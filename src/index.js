import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './components/app/App';
import { BrowserRouter } from 'react-router-dom';
import { makeServer } from './services/server';
// import { Provider } from 'react-redux';
// import { store } from './app/store';


if (process.env.NODE_ENV === 'development') {
  makeServer({ environment: 'development' });
} 

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    {/* <Provider store={store}> */}
      <App />
    {/* </Provider> */}
  </BrowserRouter>
);
